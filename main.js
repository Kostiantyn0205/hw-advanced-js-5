const addPublicationBtn = document.getElementById('addPublicationBtn');
const modal = document.getElementById('modal');
const titleInput = document.getElementById('title');
const contentInput = document.getElementById('content');
const createPublicationBtn = document.getElementById('createPublicationBtn');
const publicationsContainer = document.getElementById('cards-container');
const back = document.getElementById('back');

class Card {
    constructor(post) {
        this.post = post;
        this.element = this.createCardElement();
    }

    createCardElement() {
        const card = document.createElement('div');
        card.classList.add('card');

        const title = document.createElement('h2');
        title.textContent = this.post.title;

        const text = document.createElement('p');
        text.textContent = this.post.body;

        const authorInfo = document.createElement('div');
        authorInfo.classList.add('author-info');

        if (this.post.user) {
            const authorName = document.createElement('span');
            authorName.textContent = this.post.user.name || 'Unknown Author';
            authorName.classList.add('authorName');

            const authorEmail = document.createElement('span');
            authorEmail.textContent = this.post.user.email || 'Unknown Email';
            authorEmail.classList.add('authorEmail');

            authorInfo.appendChild(authorName);
            authorInfo.appendChild(authorEmail);
        }

        const deleteButton = document.createElement('button');
        deleteButton.textContent = 'Delete';
        deleteButton.classList.add('deleteButton');
        deleteButton.addEventListener('click', () => this.deleteCard());

        card.appendChild(authorInfo);
        card.appendChild(title);
        card.appendChild(text);
        card.appendChild(deleteButton);

        return card;
    }

    deleteCard() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.userId}`, {
            method: 'DELETE'
        })
            .then(response => {
                if (response.ok) {
                    this.element.remove();
                } else {
                    console.error('Failed to delete card');
                }
            })
            .catch(error => {
                console.error('Error deleting card:', error);
            });
    }
}

function fetchDataAndDisplayCards() {
    Promise.all([
        fetch('https://ajax.test-danit.com/api/json/users'),
        fetch('https://ajax.test-danit.com/api/json/posts')
    ])
        .then(responses => Promise.all(responses.map(response => response.json())))
        .then(([users, posts]) => {
            const cardsContainer = document.getElementById('cards-container');

            posts.forEach(post => {
                const user = users.find(user => user.id === post.userId);
                post.user = user;

                const card = new Card(post);
                cardsContainer.appendChild(card.element);
            });
        })
        .catch(error => {
            console.error('Error fetching data:', error);
        });
}

document.addEventListener('DOMContentLoaded', fetchDataAndDisplayCards);

addPublicationBtn.addEventListener('click', () => {
    modal.style.display = 'flex';
    back.style.display = 'block';
});

createPublicationBtn.addEventListener('click', () => {
    const title = titleInput.value;
    const content = contentInput.value;

    if (title && content) {
        modal.style.display = 'none';
        back.style.display = 'none';

        titleInput.value = '';
        contentInput.value = '';

        fetch('https://ajax.test-danit.com/api/json/posts', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userId: 1,
                title: title,
                body: content,
            }),
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    console.error('Помилка при створенні публікації');
                }
            })
            .then(publicationData => {
                displayPublication(publicationData);
            })
            .catch(error => {
                console.error('Виникла помилка:', error);
            });
    }
});

function displayPublication(publicationData) {
    const card = new Card(publicationData);
    publicationsContainer.prepend(card.element);
}